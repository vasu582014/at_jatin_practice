package com.day6.constructors;

public class parameterizedConstructor {
	public parameterizedConstructor(int a, int b)
	{
		int c = a+b;
		System.out.println("Multiple parameters constructor: " + c);
	}
	public static void main(String[] args) {
		parameterizedConstructor pc = new parameterizedConstructor(10,20);
	}
}
