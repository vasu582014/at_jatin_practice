package com.day6.constructors;

public class constructorOverloading {
	public constructorOverloading()
	{
		System.out.println("This is no arguments constructor");
	}
	public constructorOverloading(int a)
	{
		System.out.println("This is single Integer argument constructor: " +a);
	}
	public constructorOverloading(int a, int b)
	{
		int c = a+b;
		System.out.println("This is Two Integer arguments constructor: " +c);
	}
	public constructorOverloading(String a, String b, String c)
	{
		System.out.println("This is Three string arguments constructor: " + a + " "+ b+ " "+ c +".");
	}
	public static void main(String[] args) {
		new constructorOverloading();
		new constructorOverloading(10);
		new constructorOverloading(20,30);
		new constructorOverloading("I am Learning", "Selenium", "Automation Testing with Java");
	}
}
