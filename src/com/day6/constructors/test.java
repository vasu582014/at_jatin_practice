//Example for call Constructor inside another Constructor
package com.day6.constructors;

public class test {
	public test()
	{
		System.out.println("This is no argument constructor");
	}
	public test(int a)
	{
		this();
		System.out.println("This is single parameter constructor: " + a);
	}
	public test(int i, int j)
	{
		this(9);
		int c = i+j;
		System.out.println("This is single parameter constructor: " + c);
	}
	public static void main(String[] args) {
		new test(10,20);
	}
}
