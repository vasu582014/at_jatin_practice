//call one class Constructor inside another Class
package com.day6.constructors;

public class B extends A{
	public B()
	{
		System.out.println("B class constructor");
	}
	public static void main(String[] args) {
		new B();
	}
}
