package com.day6.constructors;

public class test2 extends test1{
	public test2()
	{
		super("Selenium");
		System.out.println("Test2 class no argument constructor");
	}
	public test2(int i)
	{
		this();
		System.out.println("Test2 class single argument constructor: " +i);
	}	
	public static void main(String[] args) {
		new test2(10);
	}
}
