//call one class Constructor inside another Class using "Super" Keyword.

package com.day6.constructors;

public class test1 {
	public test1()
	{
		System.out.println("Test1 class no argument constructor");
	}
	public test1(String s)
	{
		this();
		System.out.println("Test1 class single argument constructor: " +s);
	}
}
