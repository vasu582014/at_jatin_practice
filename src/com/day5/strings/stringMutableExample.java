package com.day5.strings;

public class stringMutableExample {
	
	public static void main(String[] args) {
		StringBuffer sb = new StringBuffer("Selenium");
		System.out.println(sb);
		sb.append(" Automation");
		System.out.println(sb);
		
		StringBuilder sb1 = new StringBuilder("Testing");
		System.out.println(sb1);
		sb1.append(" UI Part");
		System.out.println(sb1);
	}
}
