package com.day5.strings;

public class stringDemo2 {
	public static void main(String[] args) {
		String a = "Hi... I am learning Selenium Automation Testing";
		System.out.println(a);

		//Convert this string into upper case letter
		System.out.println(a.toUpperCase());

		//Convert this string into lower case letter
		System.out.println(a.toLowerCase());

		//Calculate the length of string. It counts spaces also
		System.out.println(a.length());

		//Search for the word in a string. If the word present print pass else print fail
		System.out.println(a.contains("Selenium"));
		System.out.println(a.contains("Automation"));
		if(a.contains("Selenium"))
		{
			System.out.println("Test case pass");
		}
		else
		{
			System.out.println("Test case fail");
		}
		//Add another word to the string at end
		System.out.println(a.concat("Testing"));

		System.out.println(a.substring(17));
		System.out.println(a.substring(0,16));

		//Instead of providing the index of a variable we have another method as below
		int index = a.indexOf("Selenium");
		System.out.println(index);
		String subString = a.substring(index);
		System.out.println(subString);


		String b = "        I am learining Automation";
		//I want to remove the unnecessary space in the left not the right side
		System.out.println(b);
		System.out.println(b.trim());

		String[] temp = b.split(" ");
		for(int i= 0; i<temp.length; i++)
		{
			System.out.println(temp[i]);
		}
		String c = b.replaceAll("Automation", "Selenium");
		System.out.println(c);
		System.out.println(c.replace("Selenium","Java"));

		String l = "SMILE";
		System.out.println(l);
		//Print each letter in new line

		String[] l1 = l.split("");
		for(int i= 0; i<l1.length; i++)
		{
			System.out.println(l1[i]);
		}
		String ss = "Hi i am test Hi i am doing good Hi how are you";
		System.out.println(ss);
		//Identify the duplicate words in the string

		String[] ss1 = ss.split(" ");
		int count =0;
		for(int i= 0; i<ss1.length; i++)
		{			
			for(int j = i+1; j<ss1.length; j++)
			{
				if(ss1[i].equals(ss1[j]))
				{
					count++;
				}

			}
			if(count>1)
			{
				System.out.println(ss1[i] + " : "+ count);
			}
		}

		//What are the duplicate words are there and How many ?

	}
}
