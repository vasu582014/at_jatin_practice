package com.day5.strings;

public class compareTwoStrings {
	
	public static void main(String[] args) {
		String a = "Selenium";  //Stores in String constant pool
		String b = "Selenium";  //Stores in String constant pool
		String c = new String("Selenium");  //Stores in direct heap memory
		System.out.println(a.equals(b)); //Comparing a and b
		System.out.println(a.equals(c)); //Comparing a and c
		System.out.println(a.compareTo(b)); //Comparing a and b
		System.out.println(a.compareTo(c)); //Comparing a and c
		System.out.println(a == b); //Comparing a and b
		System.out.println(a == c); //Comparing a and c
	}
}
