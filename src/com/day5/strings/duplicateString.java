//Identify the duplicate words in the string
//What are the duplicate words are there and How many ?
package com.day5.strings;

import java.util.Scanner;

public class duplicateString {
	public static void main(String[] args) {
		//Take the string from the user
		
		Scanner s = new Scanner(System.in);
		System.out.print("Enter the String: ");
		String ss = s.nextLine();
		
		//Print the user entered String
		System.out.println(ss);
		
		//Convert to the lower case letters
		ss.toLowerCase();
		
		//Split the string with space and keep it in the temp array
		String[] temp = ss.split(" ");
		
		for(int i= 0; i<temp.length; i++)
		{
			int count =1;
			for(int j = i+1; j<temp.length; j++) 
			{
				if(temp[i].equals(temp[j])) //Compare the temp[i] values with temp[j] 
				{
					count++; //if temp[i] and temp[j] is same then increase the count
					temp[j]="0"; // Make temp[j] to 0
									
				}
			}
			if(count>1 && temp[i] !="0")
			{
				System.out.println(temp[i] + " : "+ count);
			}
		}
	}
}
