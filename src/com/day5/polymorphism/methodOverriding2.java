package com.day5.polymorphism;

public class methodOverriding2 extends methodOverriding1{
	@Override
	public void test()
	{
		System.out.println("Test method from Child class");
	}
	public static void main(String[] args) {
		methodOverriding2 mo = new methodOverriding2();
		mo.test();
	}
}
