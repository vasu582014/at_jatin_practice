package com.day5.polymorphism;

public class BMW extends Car{

	public void add2()
	{
		System.out.println("Add method from BMW class");
	}
	public static void main(String[] args) {
		//Creating child class(BMW) object with child class (BMW) class reference
		BMW b = new BMW();
		b.add1();
		b.add2();

		//Creating parent class(car) object with Parent class (car) class reference
		Car c = new Car();
		c.add1();
		//c.add2();   Not possible because parent cannot access child class properties

		//Creating child class(BMW) object with Parent class (car) class reference
		Car c1= new BMW();
		c1.add1();
		//c1.add2();   Not possible because parent cannot access child class properties

		//Creating parent class(car) object with child class (BMW) class reference
		/*
		 * BMW b1 = new Car(); Not possible because parent cannot access child class
		 * properties b1.add1(); 
		 * b1.add2();
		 */ 
	}
}
