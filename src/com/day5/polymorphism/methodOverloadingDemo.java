package com.day5.polymorphism;

public class methodOverloadingDemo {
	int length;
	int width;
	public void calculateArea()  //Method 1
	{
		int totalArea = length * width;
		System.out.println("Method 1 totalArea: "+totalArea);
	}
	public void calculateArea(int length)  //Method 2
	{
		int totalArea = length*length;
		System.out.println("Method 2 totalArea: "+totalArea);
	}
	public void calculateArea(double length,double width)   //Method 3
	{
		double totalArea = length*width;
		System.out.println("Method 3 totalArea: "+totalArea);
	}
	public void calculateArea(int length,int width)   //Method 4
	{
		int totalArea = length*width;
		System.out.println("Method 4 totalArea: "+totalArea);
	}
	public static void main(String[] args) {
		methodOverloadingDemo demo = new methodOverloadingDemo();
		demo.calculateArea();  //Calling Method 1
		demo.calculateArea(10);	 //Calling Method 2
		demo.calculateArea(10.55,20.66);  //Calling Method 3
		demo.calculateArea(20, 15);  //Calling Method 4
		demo.calculateArea(200, 150, 5, 7);  //Calling Method 5
	}
	public void calculateArea(int length,int width, int a, int b)   //Method 5
	{
		int sum = length*width*a*b;
		System.out.println("Method 5 sum: "+sum);
	}
}
