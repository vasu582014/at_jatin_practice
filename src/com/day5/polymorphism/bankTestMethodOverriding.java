package com.day5.polymorphism;

public class bankTestMethodOverriding {
	public static void main(String[] args) {
		Bank b;
		b = new SBI();
		System.out.println("SBI Interest is: " + b.getRateOfInterest());
		
		b = new ICICI();
		System.out.println("ICICI Interest is: " + b.getRateOfInterest());
		
		b = new AXIS();
		System.out.println("AXIS Interest is: " + b.getRateOfInterest());
	}
}