package com.contact;

public class Runner {

	public static void main(String[] args) {

		System.out.println("---------------Welcome to Contact Application-----------------");
		/*
		 * Name name = new Name(); name.setfName("Automation");
		 * name.setlName(" Testing"); System.out.println("Name: " + name.getfName() +
		 * name.getlName());
		 * 
		 * ContactNumber contactNumber = new ContactNumber();
		 * contactNumber.setCountryCode("+91-");
		 * contactNumber.setMobileNumber("7799526729");
		 */

		//OR

		Contact contact = new Contact();
		Name contactName = new Name();
		contactName.setfName("Selenium");
		contactName.setlName("Java");
		contact.setName(contactName);

		EmailAddress ContactEmailAddress = new EmailAddress();
		ContactEmailAddress.setEmailAddress("TESTJENKINS123456@GMAIL.COM");
		contact.setEmailAddress(ContactEmailAddress);

		ContactNumber contactContactNumber = new ContactNumber();
		contactContactNumber.setCountryCode("+60");
		contactContactNumber.setMobileNumber("1126866134");
		contact.setContactNumber(contactContactNumber);

		Address contactAddress = new Address();
		contactAddress.setFlatNumber(64);
		contactAddress.setAptName("ArenaGreen");
		contactAddress.setLandmark("International Stadium");
		contactAddress.setStreetName("Not Required");
		contactAddress.setCity("BukitJalil");
		contactAddress.setState("KL");
		contactAddress.setCountry("Malaysia");
		contact.setAddress(contactAddress);

		Name Person1_name = contact.getName();
		System.out.println("Name: " +Person1_name.getfName() + " " + Person1_name.getlName());
		
		EmailAddress Person1_emailAddress = contact.getEmailAddress();
		System.out.println("Email_Address: "+ Person1_emailAddress.getEmailAddress());
		
		ContactNumber Person1_contactNumber = contact.getContactNumber();
		System.out.println("Contact_Number: "+Person1_contactNumber.getCountryCode() + "-"+Person1_contactNumber.getMobileNumber());
		
		Address Person1_address = contact.getAddress();
		System.out.println("Address: \n"+ Person1_address.getFlatNumber() + " , "+Person1_address.getAptName() + " \nLandmark: " + Person1_address.getLandmark() +", "+ Person1_address.getStreetName() +"\nCity: " + Person1_address.getCity() +"\nState: " + Person1_address.getState()+ "\nCountry: "+ Person1_address.getCountry());

		System.out.println("Contact Added Successfully");
	}
}
