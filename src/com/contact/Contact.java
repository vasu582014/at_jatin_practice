package com.contact;

public class Contact {
	Name name;
	ContactNumber contactNumber;
	EmailAddress emailAddress;
	Address address;
	public Name getName() {
		return name;
	}
	public void setName(Name name) {
		this.name = name;
	}
	public ContactNumber getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(ContactNumber contactNumber) {
		this.contactNumber = contactNumber;
	}
	public EmailAddress getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(EmailAddress emailAddress) {
		this.emailAddress = emailAddress;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
}
