package com.day1;

public class Methods {
	public void add()
	{
		System.out.println("This is Add Method logic");
	}
	public static void main(String[] args) {
		
		System.out.println("This is Main Method");
		Methods m = new Methods(); //Creating object for the class
		m.add();
		m.div();
	}
	public void mul()
	{
		System.out.println("This is Mul Method logic");
	}
	public void div()
	{
		System.out.println("This is Div method logic");
	}

}
