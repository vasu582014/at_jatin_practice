package com.day1;

public class Child extends Parent{
	public void add()
	{
		System.out.println("This is child class add Method logic");
	}
	
	public void sub()
	{
		System.out.println("This is child class sub Method logic");
	}
	
}
