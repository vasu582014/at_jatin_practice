package com.day1;

public class Parent {
	
	public void div()
	{
		System.out.println("This is Parent class Div Method logic");
	}
	
	public void mul()
	{
		System.out.println("This is Parent class Mul Method logic");
	}

}
