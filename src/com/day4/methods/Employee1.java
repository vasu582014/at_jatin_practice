package com.day4.methods;

public class Employee1 {
	public void data(String employee_Name, int employee_Number, double employee_Salary)
	{
		System.out.println("Employee Data: 	" + employee_Number + "	" + employee_Name + "	" + employee_Salary);
	}
	public static void main(String[] args) {
		Employee1 emp1 = new Employee1();
		emp1.data("Vasudha_Automation", 01 , 50000);
		emp1.data("Nirvigna_Tesitng", 02, 84000);
	}
}