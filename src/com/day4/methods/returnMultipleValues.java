package com.day4.methods;

public class returnMultipleValues {
	
	public String getUserName()
	{
		String a = "Selenium";
		return a;
	}
	public String[] getUserNames()
	{
		String[] b = new String[3];
		b[0] = "Automation";
		b[1] = "Testing";
		b[2] = "With Java";
		return b;
	}
	public static void main(String[] args) {
		returnMultipleValues r = new returnMultipleValues();
		String newVariable = r.getUserName();
		System.out.println(newVariable);
		
		String tempArray[] = r.getUserNames();
		for(int i = 0; i<tempArray.length; i++)
		{
			System.out.println(tempArray[i]);
		}
	}
}
