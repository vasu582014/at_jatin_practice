package com.day4.methods;

public class methodWithVoidReturnType_Employee {
	double Salary;
	double Bonus;
	public void calculateSalary()
	{
		double totalSalary = Salary + Bonus;
		System.out.println("Total Salary: " +totalSalary);
	}
	public static void main(String[] args) {
		methodWithVoidReturnType_Employee manoj = new methodWithVoidReturnType_Employee();
		manoj.Salary = 10000;
		manoj.Bonus = 2000;
		manoj.calculateSalary();
		
		methodWithVoidReturnType_Employee john = new methodWithVoidReturnType_Employee();
		john.Salary = 22000;
		john.Bonus = 4000;
		john.calculateSalary();
		//System.out.println(manoj.calculateSalary() + john.calculateSalary()); Not possible because method return type is void
	}
}
