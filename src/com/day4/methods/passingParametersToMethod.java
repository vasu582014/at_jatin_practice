package com.day4.methods;

public class passingParametersToMethod {
	
	public void calculateSalary(double Salary, double Bonus, double medicalAllowance)
	{
		double totalSalary = Salary + Bonus + medicalAllowance;
		System.out.println("Total Salary: "+ totalSalary);
	}
	public static void main(String[] args) {
		passingParametersToMethod John = new passingParametersToMethod();
		John.calculateSalary(10000, 1000 ,500);
	}
}
