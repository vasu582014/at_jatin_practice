package com.day4.methods;

public class methodWithValidReturnType_Employee {
	
	double Salary;
	double Bonus;
	public double calculateSalary()
	{
		double totalSalary = Salary + Bonus;
		System.out.println("Total Salary: " +totalSalary);
		return totalSalary;
	}
	public static void main(String[] args) {
		methodWithValidReturnType_Employee manoj = new methodWithValidReturnType_Employee();
		manoj.Salary = 10000;
		manoj.Bonus = 2000;
		manoj.calculateSalary();
		
		methodWithValidReturnType_Employee john = new methodWithValidReturnType_Employee();
		john.Salary = 22000;
		john.Bonus = 4000;
		john.calculateSalary();
		System.out.print(manoj.calculateSalary() + john.calculateSalary());   //Possible because method return type is Valid
	}
}
