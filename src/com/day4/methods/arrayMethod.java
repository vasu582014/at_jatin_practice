package com.day4.methods;

public class arrayMethod {
	
	public String[] getCountryName()
	{
		String[] country = new String[7];
		country[0] = "India";
		country[1] = "Pakistan";
		country[2] = "China";
		country[3] = "Malaysia";
		country[4] = "Indonesia";
		country[5] = "Hongkong";
		country[6] = "Singapore";
		return country ;
	}
	public static void main(String[] args) {
		arrayMethod am = new arrayMethod();
		String countries[] = am.getCountryName();
		for(int i =0; i<countries.length; i++)
		{
			if(countries[i] == "India")
			{
				System.out.println("Test case Passed: I am proud to be an Indian");
			}
			else
			{
				System.out.println("Test case failed");
			}
		}
	}
}
