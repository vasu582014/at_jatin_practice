package com.day4.methods;

public class passingArguments {

	public String getUserName(String firstName, String lastName)
	{
		String userName = firstName + lastName;
		return userName;
	}
	public static void main(String[] args) {
		passingArguments p = new passingArguments();
		String userFullName_First = p.getUserName("Selenium", " Automation");
		String userFullName_Second = p.getUserName("UI", " Testing");
		
		System.out.println(userFullName_First);
		System.out.println(userFullName_Second);
	}
}
