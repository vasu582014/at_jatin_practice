  /*
	 5
    454
   34543
  2345432
 123454321
 
 */

package com.day4.controlstatements;
public class nestedforloop3 {
	public static void main(String[] args) {
		for(int i = 0; i<5; i++)
		{
			for (int j=5-i; j>0; j--)
			{
				System.out.print(" ");
			}
			for(int k= 5-i;k<=5;k++)
			{
				System.out.print(k);
			}
			for(int n=5-i,m=4;n<5;n++,m--)
			{
				System.out.print(m);
			}
			System.out.println("");
		}
	}
}
