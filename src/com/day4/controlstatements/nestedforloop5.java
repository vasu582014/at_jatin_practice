package com.day4.controlstatements;

public class nestedforloop5 {
	public static void main(String[] args) {
		for(int i=5; i>=1; i--)
		{
			for(int j =1; j<=i; j++)
			{
				System.out.print("   ");
			}
			for(int k=0,m=6-i;k<=5-i;k++,m--)
			{
				System.out.print(" "+m+" ");
			}
			
			for(int n=2;n<=6-i; n++)
			{
				System.out.print(" "+n+" ");
			}
			
			System.out.println("");
		}
	}
}
