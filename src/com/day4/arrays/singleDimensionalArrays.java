package com.day4.arrays;

public class singleDimensionalArrays {
	public static void main(String[] args) {
		int[] a= new int[5];
		a[0] = 10;
		a[1] = 12;
		a[2] = 5;
		a[3] = 18;
		a[4] = 25;
		for(int i=0;i<5; i++)
		{
			System.out.println(a[i]);
		}
		System.out.println("Array Size is: " + a.length); //array.length is used to find the length if the array
	}
}
