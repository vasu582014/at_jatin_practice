package com.day4.arrays;

public class multiDimensionalArrays {
	
	public static void main(String[] args) {
		int a[][] = {{10,11,12,13,14,15},{20,21,22,23,24,25},{30,31,32,33,34,35}};
		int b[][] = new int[5][2];
		b[0][0] = 100;
		b[0][1] = 101;
		b[1][0] = 200;
		b[1][1] = 201;
		b[2][0] = 300;
		b[2][1] = 301;
		b[3][0] = 400;
		b[3][1] = 401;
		b[4][0] = 500;
		b[4][1] = 501;
		System.out.println("First array: ");
		for(int i =0; i<3;i++)
		{
			for(int j = 0; j<6; j++)
			{
				System.out.print(" " +a[i][j]);
			}
			System.out.println("");
		}
		System.out.println("Second array: ");
		for(int k =0; k<5;k++)
		{
			for(int l = 0; l<2; l++)
			{
				System.out.print(" " +b[k][l]);
			}
			System.out.println("");
		}
	}
}
