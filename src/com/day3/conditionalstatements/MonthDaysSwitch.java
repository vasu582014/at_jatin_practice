package com.day3.conditionalstatements;

import java.util.Scanner;

public class MonthDaysSwitch {
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.print("Enter the Month: ");
		String Month = s.nextLine();

		switch(Month)
		{
			case "January": 
				System.out.println("Month: January have 31 days");
				break;
			case "February":
				System.out.println("Month: February have 28 or 29 days");
				break;
			case "March":
				System.out.println("Month: March have 31 days");
				break;
			case "April":
				System.out.println("Month: April have 30 days");
				break;
			case "May":
				System.out.println("Month: May have 31 days");
				break;
			case "June":
				System.out.println("Month: June have 30 days");
				break;
			case "July":
				System.out.println("Month: July have 31 days");
				break;
			case "August":
				System.out.println("Month: August have 31 days");
				break;
			case "September":
				System.out.println("Month: September have 30 days");
				break;
			case "October":
				System.out.println("Month: October have 31 days");
				break;
			case "November":
				System.out.println("Month: November have 30 days");
				break;
			case "December":
				System.out.println("Month: December have 31 days");
				break;	
			default:	
				System.out.println("Month: " + Month + " is not a valid month !!!!!!!!");
				System.out.println("Please try again.......");
		}
	}
}
