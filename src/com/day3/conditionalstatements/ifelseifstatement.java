package com.day3.conditionalstatements;

import java.util.Scanner;

public class ifelseifstatement {

	public static void main(String[] args) {

		Scanner s =new Scanner(System.in);

		System.out.print("Enter your number: ");
		int n = s.nextInt();
		
		if(n>=1 && n<100 )
		{
			System.out.println("N is a two digit number");
		}
		else if(n>=100 && n<1000)
		{
			System.out.println("N is a three digit number");
		}	
		else if(n>=1000 && n<10000)
		{
			System.out.println("N is a four digit number");
		}
		else if(n>=10000 && n<100000)
		{
			System.out.println("N is a five digit number");
		}	
		else
		{
			System.out.println("N is not in between 1 and 99999");
		}
	}
}
