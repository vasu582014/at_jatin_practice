package com.day3.conditionalstatements;

import java.util.Scanner;

public class LeapYear {
	public static void main(String[] args) {
		Scanner s =new Scanner(System.in);

		System.out.print("Enter year Number: ");
		int Year = s.nextInt();
		if(Year%4 == 0)
		{
			System.out.println("Given number: "+ Year + " is a Leap Year");
		}
		else 
		{
			System.out.println("Given number: "+ Year + " is not a Leap Year");
		}

	}

}
