package com.day3.conditionalstatements;

import java.util.Scanner;

public class positiveornegative {
	public static void main(String[] args) {

		Scanner s =new Scanner(System.in);

		System.out.print("Enter your number: ");
		int n = s.nextInt();
		if(n%2 == 0)
		{
			System.out.println("Given number: "+ n + " is even");
		}
		else 
		{
			System.out.println("Given number: "+ n + " is odd");
		}

	}
}