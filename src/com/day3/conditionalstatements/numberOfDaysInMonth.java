package com.day3.conditionalstatements;

import java.util.Scanner;

public class numberOfDaysInMonth {
	public static void main(String[] args) {

		Scanner s = new Scanner(System.in);
		System.out.print("Enter the Month: ");
		String Month = s.nextLine();

		if(Month == "January" || Month == "March" || Month == "May" || Month == "July" || Month =="August" || Month == "October" || Month == "December") 
		{
			System.out.println("Month: " + Month + " have 31 days"); 
		} 
		else if(Month == "February") 
		{ 
			System.out.println("Month: " + Month + " have 28 or 29 days");
		} 
		else if(Month == "April" || Month == "June" || Month == "September" || Month == "November") 
		{ 
			System.out.println("Month: " + Month + " have 30 days"); 
		} 
		else 
		{ 
			System.out.println("Month: " + Month + " is not a valid month !!!!!!!!");
			System.out.println("Please try again.......");
		}

	}

}
