package com.day3.conditionalstatements;

public class switchcasestatement {
	public static void main(String[] args) {
		int n =2;
		switch(n+2)
		{
			case 0:
				System.out.println("Case 0: Value is: " + n);
				break;
			case 1:
				System.out.println("Case 1: Value is: " + n);
				break;
			case 2:
				System.out.println("Case 2: Value is: " + n);
				break;
			case 3:
				System.out.println("Case 3: Value is: " + n);
				break;
			/*
			 * case 4: System.out.println("Case 4: Value is: " + n); break;
			 */
			default:
				System.out.println("Default: Value is: " + n);
				break;
		}
	}

}
