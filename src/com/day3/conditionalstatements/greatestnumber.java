package com.day3.conditionalstatements;

import java.util.Scanner;

public class greatestnumber {
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.print("Enter the Number1: ");
		int Number1 = s.nextInt();
		
		System.out.print("Enter the Number2: ");
		int Number2 = s.nextInt();
		
		System.out.print("Enter the Number3: ");
		int Number3 = s.nextInt();
		
		if(Number1>Number2 && Number1>Number3)
		{
			System.out.println("Number1: "+Number1+ " is bigger ");
		}
		else if(Number2>Number1 && Number2>Number3)
		{
			System.out.println("Number2: "+Number2+ " is bigger ");
		}
		else if(Number3>Number1 && Number3>Number2)
		{
			System.out.println("Number3: "+Number3+ " is bigger ");
		}
		else
		{
			System.out.println("Number1, Number2 and Number3 :" + Number1 + ", " + Number2 + " & " + Number3+ " are equal");
		}
	}

}
