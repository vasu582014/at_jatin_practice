package com.day3.AccessModifiers1;

import com.day3.AccessModifiers.*;

public class E {   //Different Package and non sub class
	
	public static void main(String[] args) {	
		
		A e = new A();
		/*
		 * e.add();  Default cannot be accessible --> Different package and non sub class
		 * e.sub();  Private cannot be accessible --> Different package and non sub class
		 * e.div();	 Protected cannot be accessible --> Different package and non sub class
		 */
		e.mul();   //Public can be accessible --> Different package and non sub class
		
	}

}
