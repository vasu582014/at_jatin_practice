package com.day3.AccessModifiers1;

import com.day3.AccessModifiers.*;

public class D extends A{ //Different package and sub class
	public static void main(String[] args) {
		D d = new D();
		//d.add(); // Default cannot be accessible --> Different package and sub class
		//d.sub(); // Private cannot be accessible --> Different package and sub class
		d.div(); // Protected can be accessible --> Different package and sub class
		d.mul(); // Public can be accessible --> Different package and sub class

	}
}
