package com.day3.AccessModifiers;

public class C { //Same package and non sub class
	
	public static void main(String[] args) {
		A a = new A();
		a.add(); // Default can be accessible --> with in Same package and non sub class
		//a.sub(); // Private cannot be accessible --> with in Same package and non sub class
		a.div(); // Protected can be accessible --> with in Same package and non sub class
		a.mul(); // Public can be accessible --> with in Same package and non sub class
	}

}
