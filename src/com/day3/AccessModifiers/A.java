package com.day3.AccessModifiers;

public class A {
	
	void add()
	{
		System.out.println("Add Method with Default Access modifier");
	}
	private void sub()
	{
		System.out.println("Sub Method with Private Access modifier");
	}
	protected void div()
	{
		System.out.println("Div Method with Protected Access modifier");
	}
	public void mul() 
	{
		System.out.println("Mul Method with Public Access modifier");
	}
	public static void main(String[] args)  //Within same class
	{
		A a = new A();
		a.add();
		a.sub();
		a.div();
		a.mul();
	}
}
