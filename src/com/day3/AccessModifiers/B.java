package com.day3.AccessModifiers;

public class B extends A{   //Same package and sub class
	public static void main(String[] args) {  
		B b = new B();
		b.add(); // Default can be accessible --> with in Same package and sub class
		//b.sub(); // Private cannot be accessible --> with in Same package and sub class
		b.div(); // Protected can be accessible --> with in Same package and sub class
		b.mul(); // Public can be accessible --> with in Same package and sub class
	}
}
