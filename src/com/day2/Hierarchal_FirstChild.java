package com.day2;

public class Hierarchal_FirstChild extends Hierarchal_Parent{

	public void div()
	{
		System.out.println("FirstChild class Div method");
	}
	public static void main(String[] args) {
		Hierarchal_FirstChild h1 = new Hierarchal_FirstChild();
		h1.add();
		h1.sub();
		h1.div();
	}
}
