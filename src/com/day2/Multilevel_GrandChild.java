package com.day2;

public class Multilevel_GrandChild extends Multilevel_Child{

	public void div() 
	{
		System.out.println("GrandChild class div method");
	}
	public static void main(String[] args) {
		System.out.println("Here we call all GrandParent --> Parent --> Child --> GrandChild class properties");
		Multilevel_GrandChild m = new Multilevel_GrandChild();
		m.add();
		m.sub();
		m.mul();
		m.div();
	}
}
