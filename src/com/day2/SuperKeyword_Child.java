package com.day2;

public class SuperKeyword_Child extends SuperKeyword_Parent {

	public void Name()
	{
		System.out.println("Name method from Child class");
	}

	public void Parent_Name()
	{
		super.Name();
	}
	public static void main(String[] args) {
		
		SuperKeyword_Child c = new SuperKeyword_Child();
		c.Name();
		c.Parent_Name();
	}
}
