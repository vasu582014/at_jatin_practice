package com.day2;

public class Hierarchal_SecondChild extends Hierarchal_Parent{

	public void mul()
	{
		System.out.println("SecondChild class mul method");
	}
	public static void main(String[] args) {
		Hierarchal_SecondChild h2 = new Hierarchal_SecondChild();
		h2.add();
		h2.sub();
		h2.mul();
	}
}
