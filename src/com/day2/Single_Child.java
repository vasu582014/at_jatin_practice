package com.day2;

public class Single_Child extends Single_Parent{
	
	public void add()
	{
		System.out.println("This is child class add Method logic");
	}
	
	public void sub()
	{
		System.out.println("This is child class sub Method logic");
	}
	
	public static void main(String[] args) {
		Single_Child c = new Single_Child();
		c.add();
		c.sub();
		c.mul();
		c.div();
	}
}
