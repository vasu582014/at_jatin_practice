package com.day7.Interfaces;

public interface MSOFFICE {
	
	int a =100; //By default it is a final Public static
	
	public void open();
	public void edit();
	public void save();
	public void delete();
	
	public static void create()  //from java 1.8 onwards we can create implemented methods in java with the help of static keyword
	{
		System.out.println("Test");
	}
	public static void main(String[] args) {
		MSOFFICE m;
		m = new WORD();
		m.open();
		m.edit();
		m.save();
		m.delete();

		EXCEL e= new EXCEL();
		e.open();
		e.edit();
		e.save();
		e.delete();
		
		PAINT p = new PAINT();
		p.open();
		p.edit();
		p.save();
		p.delete();
	}
}
