package com.day7.Interfaces;

public class WORD implements MSOFFICE {

	@Override
	public void open() {
		System.out.println("This will open WORD");
		System.out.println(MSOFFICE.a);
	}

	@Override
	public void edit() {
		System.out.println("This will edit WORD");
		
	}

	@Override
	public void save() {
		System.out.println("This will save WORD");
		
	}

	@Override
	public void delete() {
		System.out.println("This will delete WORD");
		
	}

}
