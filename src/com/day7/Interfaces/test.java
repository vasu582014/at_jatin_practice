package com.day7.Interfaces;

public interface test {

	/* Implemented Method --> Not possible in Interface
	public void add()
	{
		
	}
	 
	*/ 
	
	//Unimplemented methods
	public void add();
	public void sub();
	public void mul();
	public void div();
	public static void main(String[] args) {
		test t = new interfaceTest();
		t.add();
		t.sub();
		t.mul();
		t.div();
	}
}
