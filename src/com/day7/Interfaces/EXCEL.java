package com.day7.Interfaces;

public class EXCEL implements MSOFFICE{

	@Override
	public void open() {
		System.out.println("This will open EXCEL");
		
	}

	@Override
	public void edit() {
		System.out.println("This will edit EXCEL");
		
	}

	@Override
	public void save() {
		System.out.println("This will save EXCEL");
		
	}

	@Override
	public void delete() {
		System.out.println("This will delete EXCEL");
		
	}

}
