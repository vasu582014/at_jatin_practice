package com.day7.Interfaces;

public class PAINT implements MSOFFICE {

	@Override
	public void open() {
		System.out.println("This will open PAINT");

	}

	@Override
	public void edit() {
		System.out.println("This will edit PAINT");

	}

	@Override
	public void save() {
		System.out.println("This will save PAINT");

	}

	@Override
	public void delete() {
		System.out.println("This will delete PAINT");

	}

}
