package com.day7.finalKeyword;

public class finalVariable {
	final int a = 1000;
	final public void method1()
	{
		System.out.println("A: "+a);
	}
	public static void main(String[] args) {
		//a = 500;
		finalVariable f = new finalVariable();
		f.method1();
	}
}
